<#
.SYNOPSIS
  Install newest version of OpenJDK
.DESCRIPTION
  Script will install OpenJDK 23, unzip it to desired folder (C:\Program Files\OpenJDK\)
.PARAMETER <Parameter_Name>
    None
.INPUTS
  None
.OUTPUTS
  Log file stored in C:\Windows\Temp\install_openjdk.log
.NOTES
  Version:        2025-01-12
  Author:         Piotr Berent
  Creation Date:  12.01.2025
  Purpose/Change: Initial script development
  
.EXAMPLE
  install_openjdk.ps1
#>

#---------------------------------------------------------[Initialisations]--------------------------------------------------------
#Requires -RunAsAdministrator
#Set Error Action to Silently Continue
$ErrorActionPreference = "SilentlyContinue"

#-----------------------------------------------------------[Functions]------------------------------------------------------------

function Unzip($zipfile, $outdir)
{
    Add-Type -AssemblyName System.IO.Compression.FileSystem
    $archive = [System.IO.Compression.ZipFile]::OpenRead($zipfile)
    foreach ($entry in $archive.Entries)
    {
        $entryTargetFilePath = [System.IO.Path]::Combine($outdir, $entry.FullName)
        $entryDir = [System.IO.Path]::GetDirectoryName($entryTargetFilePath)
        
        #Ensure the directory of the archive entry exists
        if(!(Test-Path $entryDir )){
            New-Item -ItemType Directory -Path $entryDir | Out-Null 
        }
        
        #If the entry is not a directory entry, then extract entry
        if(!$entryTargetFilePath.EndsWith("\")){
            [System.IO.Compression.ZipFileExtensions]::ExtractToFile($entry, $entryTargetFilePath, $true);
        }
    }
}

function ReloadPath {
  $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") +
              ";" +
              [System.Environment]::GetEnvironmentVariable("Path","User")
}

Function Get-DateFormat 
{
    Get-Date -Format "yyyy-MM-dd-HH-mm-ss"
}

Function Write-Log($Type, $EventCustomEntry)
{
    If ($Type -eq "Info")
    {
        If ($Null -ne $LogPath)
        {
            Add-Content -Path $Log -Encoding ASCII -Value "$(Get-DateFormat) [INFO] $EventCustomEntry"
        }
        Write-Host "$(Get-DateFormat) [INFO] $EventCustomEntry"
    }

    If ($Type -eq "Succ")
    {
        If ($Null -ne $LogPath)
        {
            Add-Content -Path $Log -Encoding ASCII -Value "$(Get-DateFormat) [SUCCESS] $EventCustomEntry"
        }
        Write-Host -ForegroundColor Green "$(Get-DateFormat) [SUCCESS] $EventCustomEntry"
    }

    If ($Type -eq "Err")
    {
        If ($Null -ne $LogPath)
        {
            Add-Content -Path $Log -Encoding ASCII -Value "$(Get-DateFormat) [ERROR] $EventCustomEntry"
        }
        Write-Host -ForegroundColor Red -BackgroundColor Black "$(Get-DateFormat) [ERROR] $EventCustomEntry"
    }

    If ($Type -eq "Conf")
    {
        If ($Null -ne $LogPath)
        {
            Add-Content -Path $Log -Encoding ASCII -Value "$EventCustomEntry"
        }
        Write-Host -ForegroundColor Cyan -Object "$EventCustomEntry"
    }
}
#----------------------------------------------------------[Declarations]----------------------------------------------------------

#Log File Info
$LogPath = "C:\Windows\Temp"

If ($LogPath)
{
    $LogDate = Get-DateFormat
    $LogFile = "install_openjdk_$LogDate.log"
    $Log = "$LogPath\$LogFile"
    $LogT = Test-Path -Path $Log
    If ($LogT)
    {
        Clear-Content -Path $Log
    }
    Add-Content -Path $Log -Encoding ASCII -Value "$(Get-DateFormat) [INFO] Log started"
}

#-----------------------------------------------------------[Execution]------------------------------------------------------------
Write-Log -Type Info -Event "Closing JAVA applications"
Get-Process -Name jucheck | Stop-Process -Force
Get-Process -Name jusched | Stop-Process -Force
Get-Process -Name jawaw | Stop-Process -Force
Get-Process -Name java | Stop-Process -Force
Get-Process -Name javaws | Stop-Process -Force
Write-Log -Type Info -Event "All JAVA apps were closed"
Write-Log -Type Info -Event "Uninstalling Oracle Java"
Get-WmiObject Win32_Product -filter "name like 'Java%' AND vendor like 'Oracle%'" | ForEach-Object { $_.Uninstall() } | Out-Null
$TestOracleJava = Get-WmiObject Win32_Product -filter "name like 'Java%' AND vendor like 'Oracle%'"
If ($Null -ne $TestOracleJava)
  {
    Write-Log -Type Err -Event "Oracle Java is still installed - check Control Panel"
  }
else {
  Write-Log -Type Succ -Event "Oracle Java unsintalled"
}  
Write-Log -Type Info -Event "Looking for old OpenJDK java"
$IfJava = Get-Command java | Select-Object Source
If ($IfJava){
  $OldJavaPath = $IfJava[0].Source.ToString()
  $OldJavaDir = $OldJavaPath.Substring(0, $OldJavaPath.LastIndexOf('\'))
  Write-Log -Type Info -Event "java.exe found in $OldJavaDir"
  $Path = [System.Environment]::GetEnvironmentVariable('PATH','Machine')
  $Path = ($Path.Split(';') | Where-Object { $_ -ne $OldJavaDir }) -join ';'
  [System.Environment]::SetEnvironmentVariable('PATH',$Path,'Machine')
  ReloadPath
}
Else {
  Write-Log -Type Info -Event "java.exe not found"
}
Write-Log -Type Info -Event "Downloading newest stable OpenJDK"
$WebClient = New-Object System.Net.WebClient
$WebClient.DownloadFile("https://download.java.net/java/GA/jdk23.0.1/c28985cbf10d4e648e4004050f8781aa/11/GPL/openjdk-23.0.1_windows-x64_bin.zip","$ENV:TEMP\openjdk23.zip")
Write-Log -Type Info -Event "Downloaded newest stable OpenJDK"
New-Item -ItemType Directory -Path "$ENV:ProgramFiles\openjdk23" | Out-Null
Write-Log -Type Info -Event "Installing newest stable OpenJDK"
Unzip -zipfile "$ENV:TEMP\openjdk23.zip" -outdir "$ENV:ProgramFiles\openjdk23"
$javaexefile = get-childitem -path 'C:\Program Files\openjdk23' -Include *.exe -File -Recurse | Where-Object { $_.Name -eq 'java.exe' }
$javapath = ($javaexefile.DirectoryName).ToString()
[Environment]::SetEnvironmentVariable("Path", $env:Path + ";$javapath", "Machine")
ReloadPath
$javatest = Get-Command java | Select-Object Version
$javaversion = $javatest[0].Version
if ($Null -eq $javaversion) {
  Write-Log -Type Err -Event "No Java detected"
} 
else { 
  Write-Log -Type Succ -Event "Java $javaversion is installed" 
}
If ($LogPath)
{
    Add-Content -Path $Log -Encoding ASCII -Value "$(Get-Date -Format "yyyy-MM-dd HH:mm:ss") [INFO] Log finished"
}