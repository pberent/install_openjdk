# install OpenJDK on Windows

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)

## About <a name = "about"></a>

This is simple PSH script to download OpenJDK 23.0.1 for Windows 10 and Windows 11 machines

## Getting Started <a name = "getting_started"></a>

Script will uninstall Oracle JAVA, and most likely also older OpenJDK versions & install OpenJDK 23.0.1, unzip it to desired folder (C:\Program Files\OpenJDK\).</br>
Script will also set PATH enviromental variable with path to folder C:\Program Files\OpenJDK

### Prerequisites

Powershell 5.1 and above </br>
Windows 10 (most likely also on any Windows with at least PowerShell 5.1)

### Installing

Just run the script

## Usage <a name = "usage"></a>

No parameters - just run the script preferably "RunAs Administrator"

PS C:\Scripts>install_openjdk.ps1 
